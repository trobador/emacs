(setq user-emacs-directory (file-name-directory load-file-name))
(add-to-list 'load-path (concat user-emacs-directory "lisp/"))
(setq custom-file (concat user-emacs-directory "custom.el"))
(load custom-file 'noerror)
(setq-default inhibit-splash-screen t)
(setq initial-scratch-message nil)
(setq initial-major-mode 'text-mode)
(setq default-major-mode 'text-mode)
(setq ispell-dictionary "en_GB")
(add-hook 'text-mode-hook 'flyspell-mode)
(setq require-final-newline t)
(setq frame-title-format '(:eval (if buffer-file-name "%b %* %F" "%F")))
(windmove-default-keybindings)
(setq confirm-kill-emacs 'y-or-n-p)
(setq backup-directory-alist `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms `((".*" ,temporary-file-directory t)))
(setq create-lockfiles nil)
(global-set-key (kbd "C-x C-g") 'keyboard-quit)
(global-set-key (kbd "C-c C-g") 'keyboard-quit)
(global-set-key (kbd "C-x C-b") 'ibuffer)
(global-set-key (kbd "S-C-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "S-C-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "S-C-<down>") 'shrink-window)
(global-set-key (kbd "S-C-<up>") 'enlarge-window)
(global-set-key (kbd "C-+") #'text-scale-increase)
(global-set-key (kbd "C--") #'text-scale-decrease)
(global-set-key (kbd "C-0") #'text-scale-adjust)

(update-glyphless-char-display 'glyphless-char-display-control '((format-control . hex-code) (no-font . hex-code)))
(setq-default indicate-empty-lines t)
(setq-default tab-width 2)
(setq-default fill-column 100)
(setq-default js-indent-level 2)
;; (setq-default indent-tabs-mode nil)
(setq-default tab-always-indent nil)
(setq-default c-tab-always-indent nil)
(set-face-attribute 'default nil :font "Source Code Pro-10:weight=semi-bold")
(add-to-list 'custom-theme-load-path (concat user-emacs-directory "themes/"))
(load-theme 'abelson t)
(set-face-attribute 'show-paren-match nil :weight 'bold)
(tool-bar-mode -1)
(setq x-gtk-use-system-tooltips nil)
(column-number-mode t)
(add-hook 'prog-mode-hook #'display-line-numbers-mode)
(add-hook 'html-mode-hook #'display-line-numbers-mode)
(add-hook 'conf-mode-hook #'display-line-numbers-mode)
(setq show-paren-delay 0)
(show-paren-mode 1)
(global-hl-line-mode t)
;; (setq-default mode-line-format (delete '(vc-mode vc-mode) mode-line-format))
(setq auto-revert-check-vc-info t)
; Workaround for https://github.com/magit/magit/issues/3878
(add-hook 'smerge-mode-hook (lambda () (setq auto-revert-check-vc-info (not smerge-mode))))

(mouse-avoidance-mode 'cat-and-mouse)
(setq mouse-yank-at-point t
      mouse-wheel-scroll-amount '(2 ((shift) . 1) ((control)))
      mouse-wheel-progressive-speed nil
      scroll-conservatively 1000
      select-enable-primary t)

(setq help-window-select t
      temp-buffer-resize-mode t)

(require 'recentf)
(recentf-mode 1)

(require 'package)
(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")))
(package-initialize)

(unless (package-installed-p 'use-package)
  (with-timeout (10 (error "Connection timeout while refreshing package archive; aborting initialization"))
    (progn (package-refresh-contents)))
  (package-install 'use-package))
(require 'use-package)
(require 'use-package-ensure)
(setq use-package-always-ensure t)

(use-package ivy
  :init
  (setq ivy-use-virtual-buffers t)
  (global-set-key (kbd "C-c v") 'ivy-push-view)
  (global-set-key (kbd "C-c V") 'ivy-pop-view)
  :config
  (ivy-mode 1))

(use-package counsel
  :after (ivy)
  :config
  (counsel-mode))

(use-package projectile
  :config
  (projectile-mode))

(use-package ivy-rich
  :after (ivy counsel)
  :config
  (ivy-rich-mode 1)
  ;; (ivy-set-display-transformer 'ivy-switch-buffer 'ivy-rich-switch-buffer-transformer)
  (setq ivy-format-function #'ivy-format-function-line))

(use-package perspective
  :after (ivy)
	:custom
	(persp-suppress-no-prefix-key-warning t)
  :init
  (persp-mode))

(use-package magit
  :config
  ;; Workaround for https://github.com/magit/magit/issues/3878
  (add-hook 'smerge-mode-hook (lambda () (setq auto-revert-check-vc-info (not smerge-mode)))))

(use-package undo-fu) ;; Emacs 28 comes with undo-redo, which is already supported by evil

(use-package evil
  :after (counsel projectile magit perspective)
  :init
  (setq evil-want-C-u-scroll t
      evil-undo-system 'undo-fu)
  :config
  (evil-set-leader '(normal visual) (kbd "\\"))
  (evil-set-leader '(normal visual) (kbd "<leader>\\") t) ; set localleader
  (evil-define-key '(normal visual) 'global
    (kbd "<leader><SPC>") 'untabify
    (kbd "<leader><tab>") 'tabify
    (kbd "<leader>c") 'evil-ex-nohighlight
    (kbd "<leader>v") 'whitespace-mode
    (kbd "<leader>f") 'counsel-git
    (kbd "<leader>g") 'counsel-rg
    (kbd "<leader>p") 'persp-switch
    (kbd "<leader>P") 'persp-switch-last
    (kbd "<leader>r") 'projectile-replace
    (kbd "<leader>m") 'magit-dispatch
    (kbd "<leader>M") 'magit-file-dispatch
    (kbd "<leader>n") (lambda ()
                        (interactive)
                        (message (buffer-file-name))))
  (fset 'evil-visual-update-x-selection 'ignore)
  (evil-mode 1)
  (evil-select-search-module 'evil-search-module 'evil-search)
  (define-key key-translation-map (kbd "C-c") (lambda (prompt) (cond ((or (evil-insert-state-p) (evil-replace-state-p) (evil-visual-state-p)) [escape]) (t (kbd "C-c")))))
  (define-key evil-normal-state-map (kbd "g j") 'evil-next-visual-line)
  (define-key evil-normal-state-map (kbd "g k") 'evil-previous-visual-line)
  (define-key evil-normal-state-map (kbd "g l") 'persp-ivy-switch-buffer)
  (define-key evil-normal-state-map (kbd "g L") (lambda () (interactive) (persp-ivy-switch-buffer t)))
  (define-key evil-normal-state-map (kbd "g SPC") (lambda () (interactive) (switch-to-buffer (other-buffer (current-buffer) 1))))
  (define-key evil-normal-state-map (kbd "g b") 'next-buffer)
  (define-key evil-normal-state-map (kbd "g B") 'previous-buffer)
  (define-key evil-visual-state-map (kbd "P") (lambda () (interactive) (let ((evil-this-register ?0)) (call-interactively 'evil-paste-after))))
  (add-hook 'prog-mode-hook #'(lambda () (modify-syntax-entry ?_ "w")))
  (evil-set-initial-state 'xref--xref-buffer-mode 'emacs)
  (setq evil-normal-state-tag (propertize " <N> " 'face '((:background "DarkGoldenrod2" :foreground "black")))
        evil-emacs-state-tag (propertize " <E> " 'face '((:background "SkyBlue2" :foreground "black")))
        evil-insert-state-tag (propertize " <I> " 'face '((:background "chartreuse3" :foreground "black")))
        evil-replace-state-tag (propertize " <R> " 'face '((:background "chocolate" :foreground "black")))
        evil-motion-state-tag (propertize " <M> " 'face '((:background "plum3" :foreground "black")))
        evil-visual-state-tag (propertize " <V> " 'face '((:background "MediumOrchid" :foreground "black")))
        evil-operator-state-tag (propertize " <O> " 'face '((:background "sandy brown" :foreground "black")))))

(use-package evil-surround
  :after (evil)
  :config
  (global-evil-surround-mode 1))

(use-package origami
  :config
  (global-origami-mode))

(use-package ws-butler
  :config
  (add-hook 'prog-mode-hook #'ws-butler-mode))

(use-package ediff
  :init
  (add-hook 'ediff-after-quit-hook-internal 'winner-undo)
  :config
  (setq ediff-window-setup-function 'ediff-setup-window-plain
        ediff-split-window-function 'split-window-horizontally))

(use-package treemacs
  :config
  (setq treemacs-width 30)
  :custom-face
  (treemacs-root-face ((t (:inherit font-lock-constant-face :underline t :weight bold))))
  :bind
  ([f8] . treemacs))

(use-package treemacs-all-the-icons
  :after (treemacs)
  :config
  (treemacs-load-theme 'all-the-icons))

(use-package treemacs-evil
  :after (treemacs evil)
  :config
  (define-key evil-treemacs-state-map (kbd "SPC") #'treemacs-RET-action)
  (define-key evil-treemacs-state-map (kbd "a")   #'treemacs-add-project-to-workspace)
  (define-key evil-treemacs-state-map (kbd "e")   #'treemacs-edit-workspaces)
  (define-key evil-treemacs-state-map (kbd "W")   #'treemacs-switch-workspace))

(use-package treemacs-projectile
  :after (treemacs projectile))

(use-package treemacs-magit
  :after (treemacs magit))

(use-package rainbow-delimiters
  :config
  (add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
  (add-hook 'html-mode-hook 'rainbow-delimiters-mode))

(use-package hl-todo
  :config
  (global-hl-todo-mode 1))

(use-package company
  :config
  (add-hook 'prog-mode-hook 'company-mode)
  (add-hook 'html-mode-hook 'company-mode))

(defun build-starlist (n k s)
  (mapcar (lambda (x)
            (cons
             (concat (make-string x ?*) k)
             (append (apply #'append (make-list (- x 1) (list ?\s '(Br . Bl)))) (list s))))
          (number-sequence 1 n)))

(use-package org
  :after (evil)
  :config
  (setq org-startup-indented t
        org-hide-leading-stars nil ; this is important for build-starlist
        org-indent-mode-turns-on-hiding-stars nil
        org-M-RET-may-split-line nil)
  (define-key org-mode-map (kbd "S-C-<up>") nil)
  (define-key org-mode-map (kbd "S-C-<down>") nil)
  (evil-define-key 'normal org-mode-map
    (kbd "<localleader>ci") 'org-clock-in
    (kbd "<localleader>co") 'org-clock-out
    (kbd "<localleader>cu") 'org-clock-update-time-maybe
    (kbd "<localleader>cr") 'org-clock-report)
  :hook ((org-mode . visual-line-mode)
         (org-mode . (lambda ()
                       (setq prettify-symbols-unprettify-at-point 'right-edge
                             prettify-symbols-alist ; × ✓ ☐ ☑ ☇ ↘ » ☼ ¤ ⌂ • ◌ ∙ ○ ● ◉ ⋄ ◆ ♦ ◊
                             (append
                               (build-starlist 10 "" ?☼)
                               (build-starlist 10 " TODO" ?○)
                               (build-starlist 10 " DONE" ?●)
                               '(("[ ]" . ?☐)
                                 ("[X]" . ?☑)
                                 ("[-]" . ?↘))))
                      (prettify-symbols-mode)))))

(load (concat user-emacs-directory "extra.el"))
