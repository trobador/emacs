;; (use-package lsp-mode
;;   :disabled
;;   :config
;;   (add-hook 'elixir-mode-hook #'lsp)
;;   (add-hook 'html-mode-hook #'lsp)
;;   (add-hook 'css-mode-hook #'lsp)
;;   (add-hook 'js-mode-hook #'lsp)
;;   (add-hook 'vue-mode-hook #'lsp))

(use-package olivetti
  :after (evil)
  :config
  (add-hook 'markdown-mode-hook 'olivetti-mode)
  (evil-define-minor-mode-key '(normal visual) 'olivetti-mode
    (kbd "k") 'evil-previous-visual-line
    (kbd "j") 'evil-next-visual-line))

(use-package vue-mode
  :disabled
  :config
  (add-hook 'vue-mode-hook (lambda () (setq syntax-ppss-table nil)))) ; fixes vue-mode indentation bug

(use-package slime
  :disabled
  :custom
  (inferior-lisp-program "/usr/bin/sbcl")
  (slime-auto-connect 'ask))

(use-package geiser
  :disabled
  :custom
  (geiser-mode-smart-tab-p t))

(use-package lispy
  :disabled
  :config
  (add-hook 'emacs-lisp-mode-hook #'lispy-mode)
  (add-hook 'lisp-mode-hook #'lispy-mode)
  (add-hook 'scheme-mode-hook #'lispy-mode)
  (define-key lispy-mode-map "[" 'lispy-brackets)
  (define-key lispy-mode-map "]" 'lispy-right-nostring)
  (define-key lispy-mode-map "}" 'lispy-right-nostring)
  (define-key lispy-mode-map "%" 'lispy-different)
  (define-key lispy-mode-map "d" nil))

(use-package lispyville
  :disabled
  :config
  (add-hook 'lispy-mode-hook #'lispyville-mode))

(use-package elixir-mode
  :disabled
  :config
  (add-hook 'elixir-mode-hook (lambda () (setq indent-tabs-mode nil))))

(use-package emmet-mode
  :disabled
  :config
  (add-hook 'sgml-mode-hook 'emmet-mode)
  (add-hook 'css-mode-hook  'emmet-mode)
  (add-hook 'emmet-mode-hook (lambda () (setq emmet-indent-after-insert nil)))
  (add-hook 'emmet-mode-hook (lambda () (setq emmet-indentation 2))))

(use-package dockerfile-mode
  :disabled
  :config
  (add-to-list 'auto-mode-alist '("Dockerfile\\'" . dockerfile-mode)))

(use-package scala-mode
  :interpreter
  ("scala" . scala-mode))



;; Enable defer and ensure by default for use-package
;; Keep auto-save/backup files separate from source code:  https://github.com/scalameta/metals/issues/1027
(setq ;use-package-always-defer t
      ;use-package-always-ensure t
      backup-directory-alist `((".*" . ,temporary-file-directory))
      auto-save-file-name-transforms `((".*" ,temporary-file-directory t)))

;; Enable sbt mode for executing sbt commands
;(use-package sbt-mode
;  :commands sbt-start sbt-command
;  :config
;  ;; WORKAROUND: https://github.com/ensime/emacs-sbt-mode/issues/31
;  ;; allows using SPACE when in the minibuffer
;  (substitute-key-definition
;   'minibuffer-complete-word
;   'self-insert-command
;   minibuffer-local-completion-map)
;   ;; sbt-supershell kills sbt-mode:  https://github.com/hvesalai/emacs-sbt-mode/issues/152
;   (setq sbt:program-options '("-Dsbt.supershell=false"))
;)

;; Enable nice rendering of diagnostics like compile errors.

(use-package flycheck
  :init (global-flycheck-mode))

(use-package lsp-mode
  :after (evil-leader)
  ;; Optional - enable lsp-mode automatically in scala files
  :hook  (scala-mode . lsp)
         (rustic . lsp)
         ;(lsp-mode . lsp-lens-mode) ; ??? Not needed anymore?
  :config
  (evil-leader/set-key "l" lsp-command-map)
  :custom
  (gc-cons-threshold 100000000) ;; 100mb
  (read-process-output-max (* 1024 1024)) ;; 1mb
  (lsp-idle-delay 0.500)
  (lsp-log-io nil)
  (lsp-completion-provider :capf)
  (lsp-headerline-breadcrumb-segments '(symbols))
  (lsp-enable-snippet nil)
  (lsp-eldoc-enable-hover nil) ;; there is an issue with lsp-mode & eldoc where sometimes the info is not updated
  (lsp-enable-symbol-highlighting nil)
  (lsp-prefer-flymake nil))

;; Add metals backend for lsp-mode
(use-package lsp-metals
  :custom
  (lsp-response-timeout 30)
  (lsp-metals-server-args '("-J-Dmetals.allow-multiline-string-formatting=off"))
  ;(lsp-metals-treeview-show-when-views-received t) ;; Issue with Treemacs (lsp-metals#14)
  :hook (scala-mode . lsp))

;(use-package hydra)
;(defhydra hydra-lsp (:exit t :hint nil)
;  "
; Buffer^^               Server^^                   Symbol
;-------------------------------------------------------------------------------------
; [_f_] format           [_M-r_] restart            [_d_] declaration  [_i_] implementation  [_o_] documentation
; [_m_] imenu            [_S_]   shutdown           [_D_] definition   [_t_] type            [_r_] rename
; [_x_] execute action   [_M-s_] describe session   [_R_] references   [_s_] signature"
;  ("d" lsp-find-declaration)
;  ("D" lsp-ui-peek-find-definitions)
;  ("R" lsp-ui-peek-find-references)
;  ("i" lsp-ui-peek-find-implementation)
;  ("t" lsp-find-type-definition)
;  ("s" lsp-signature-help)
;  ("o" lsp-describe-thing-at-point)
;  ("r" lsp-rename)
;
;  ("f" lsp-format-buffer)
;  ("m" lsp-ui-imenu)
;  ("x" lsp-execute-code-action)
;
;  ("M-s" lsp-describe-session)
;  ("M-r" lsp-restart-workspace)
;  ("S" lsp-shutdown-workspace))

;(use-package hydra)
;(defhydra hydra-lsp (:exit t :hint nil)
;  "
; Buffer^^               Server^^                   Symbol
;-------------------------------------------------------------------------------------
; [_f_] format           [_M-r_] restart            [_d_] declaration  [_i_] implementation  [_o_] documentation
; [_m_] imenu            [_S_]   shutdown           [_D_] definition   [_t_] type            [_r_] rename
; [_x_] execute action   [_M-s_] describe session   [_R_] references   [_s_] signature"
;  ("d" lsp-find-declaration)
;  ("D" lsp-ui-peek-find-definitions)
;  ("R" lsp-ui-peek-find-references)
;  ("i" lsp-ui-peek-find-implementation)
;  ("t" lsp-find-type-definition)
;  ("s" lsp-signature-help)
;  ("o" lsp-describe-thing-at-point)
;  ("r" lsp-rename)
;
;  ("f" lsp-format-buffer)
;  ("m" lsp-ui-imenu)
;  ("x" lsp-execute-code-action)
;
;  ("M-s" lsp-describe-session)
;  ("M-r" lsp-restart-workspace)
;  ("S" lsp-shutdown-workspace))

;; Enable nice rendering of documentation on hover
;;   Warning: on some systems this package can reduce your emacs responsiveness significally.
;;   (See: https://emacs-lsp.github.io/lsp-mode/page/performance/)
;;   In that case you have to not only disable this but also remove from the packages since
;;   lsp-mode can activate it automatically.
(use-package lsp-ui
  :after (lsp-mode)
  :custom-face
  (lsp-ui-doc-background
    ,'((((background light)) :background "white")
          (t :background "black")))
  :custom
  (lsp-ui-doc-show-with-cursor nil)
  (lsp-ui-doc-border "blue")
;        :commands lsp-ui-doc-hide
;        :bind (:map lsp-ui-mode-map
;                    ([remap xref-find-definitions] . lsp-ui-peek-find-definitions)
;                    ([remap xref-find-references] . lsp-ui-peek-find-references)
;                    ("C-c u" . lsp-ui-imenu))
;        :init (setq lsp-ui-doc-enable t
;                      lsp-ui-doc-use-webkit nil
;                      lsp-ui-doc-header nil
;                      lsp-ui-doc-delay 0.2
;                      lsp-ui-doc-include-signature t
;                      lsp-ui-doc-alignment 'at-point
;                      lsp-ui-doc-use-childframe nil
;                      lsp-ui-doc-border (face-foreground 'default)
;                      lsp-ui-peek-enable t
;                      lsp-ui-peek-show-directory t
;                      lsp-ui-sideline-update-mode 'line
;                      lsp-ui-sideline-enable t
;                      lsp-ui-sideline-show-code-actions t
;                      lsp-ui-sideline-show-hover nil
;                      lsp-ui-sideline-ignore-duplicate t)
;        :custom-face
;        (lsp-ui-doc-border ((t (:background ,(face-foreground 'tooltip)))))
;        (lsp-ui-doc-background ((t (:background ,(face-background 'tooltip)))))
;        :custom
;        (lsp-ui-doc-border (face-foreground 'tooltip))
;        :config
;        ;; Reset `lsp-ui-doc-background' after loading theme
;        (add-hook 'after-load-theme-hook
;                  (lambda ()
;                    (setq lsp-ui-doc-border (face-foreground 'tooltip))
;                    (set-face-background 'lsp-ui-doc-foreground (face-foreground 'tooltip))
;                    (set-face-background 'lsp-ui-doc-background (face-background 'tooltip))))
)

;; lsp-mode supports snippets, but in order for them to work you need to use yasnippet
;; If you don't want to use snippets set lsp-enable-snippet to nil in your lsp-mode settings
;;   to avoid odd behavior with snippets and indentation
; (use-package yasnippet)

;; Add company-lsp backend for metals.
;;   (depending on your lsp-mode version it may be outdated see:
;;    https://github.com/emacs-lsp/lsp-mode/pull/1983)
; (use-package company-lsp)

;; Use the Debug Adapter Protocol for running tests and debugging
;(use-package posframe
;  ;; Posframe is a pop-up tool that must be manually installed for dap-mode
;  )
;(use-package dap-mode
;  :hook
;  (lsp-mode . dap-mode)
;  (lsp-mode . dap-ui-mode)
;  )

(use-package rustic
  :custom
  (rustic-format-trigger 'on-save)
  (add-hook 'rust-mode-hook (lambda () (prettify-symbols-mode)))
  (add-hook 'rust-mode-hook #'lsp))

(use-package flycheck-rust
  :config
  (with-eval-after-load 'rustic
    (add-hook 'flycheck-mode-hook #'flycheck-rust-setup)))
